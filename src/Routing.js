import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import Splashscreen from './screen/Splashscreen';
import AuthNavigation from './AuthNavigation';
import BottomNavigation from './BottomNavigation';
import HomeNavigation from './HomeNavigation';
import TransactionNavigation from './TransactionNavigation';
import ProfileNavigation from './ProfileNavigation';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, statusBarColor: 'black'}}>
        <Stack.Screen name="SplashScreen" component={Splashscreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="BottomNavigation" component={BottomNavigation} />
        <Stack.Screen name="HomeNavigation" component={HomeNavigation} />
        <Stack.Screen
          name="TransactionNavigation"
          component={TransactionNavigation}
        />
        <Stack.Screen name="ProfileNavigation" component={ProfileNavigation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
