import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Profilescreen from './screen/Profilescreen';
import EditProfilescreen from './screen/Editprofile';
import FAQscreen from './screen/Faq';

const Stack = createStackNavigator();

function ProfileNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Profile" component={Profilescreen} />
      <Stack.Screen
        name="Edit Profile"
        component={EditProfilescreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="FAQ"
        component={FAQscreen}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

export default ProfileNavigation;
