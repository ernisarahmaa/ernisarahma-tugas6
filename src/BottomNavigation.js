import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeNavigation from './HomeNavigation';
import TransactionNavigation from './TransactionNavigation';
import ProfileNavigation from './ProfileNavigation';
import Detailscreen from './screen/Detailscreen';

const Tab = createBottomTabNavigator();

function TabMenu({isFocused, label}) {
  const icon = getIcon(label);
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={icon}
        style={{
          width: label === 'Transaction' ? 47 : 20,
          height: label === 'Transaction' ? 50 : 20,
          resizeMode: 'contain',
          tintColor: isFocused ? 'red' : 'grey',
        }}
      />
      <Text
        style={{
          color: isFocused ? 'red' : 'grey',
          marginTop: label === 'Transaction' ? -16 : 9,
          paddingBottom: label === 'Transaction' ? 5 : 0,
        }}>
        {label}
      </Text>
    </View>
  );
}

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#fff',
              height: 70,
            }}>
            <TabMenu isFocused={isFocused} label={label} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const getIcon = label => {
  switch (label) {
    case 'Home':
      return require('../src/assets/icons/home.png');
    case 'Transaction':
      return require('../src/assets/icons/transaction.png');
    case 'Profile':
      return require('../src/assets/icons/profile.png');
  }
};

export default function BottomNavigation() {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={HomeNavigation} />
      <Tab.Screen name="Transaction" component={TransactionNavigation} />
      <Tab.Screen name="Profile" component={ProfileNavigation} />
    </Tab.Navigator>
  );
}
