import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';

const Cartscreen = ({navigation, route}) => {
  const {order} = useSelector(state => state.order);
  console.log('cek cart', order);

  const serviceTextMap = {
    gantiSol: 'Ganti Sol Sepatu',
    jahitSepatu: 'Jahit Sepatu',
    repaintSepatu: 'Repaint Sepatu',
    cuciSepatu: 'Cuci Sepatu',
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      {/* <ScrollView> */}
      <FlatList
        style={{
          marginTop: 9,
          marginHorizontal: 11,
        }}
        data={order}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Order Form', {item})}
            style={{
              width: '100%',
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderRadius: 8,
              marginBottom: 5,
              paddingVertical: 25,
            }}>
            <Image
              style={{width: 85, height: 85, marginHorizontal: 11}}
              source={require('../assets/images/shoes1.png')}
            />
            <View style={{justifyContent: 'space-evenly', flex: 1}}>
              <Text style={{fontWeight: '500', color: '#000'}}>
                {item.merk} - {item.color} - {item.size}
              </Text>
              <Text>{serviceTextMap[item.isService]}</Text>
              <Text>Note : {item.note}</Text>
            </View>
          </TouchableOpacity>
        )}
      />

      <TouchableOpacity
        onPress={() => navigation.navigate('Order Form')}
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          marginVertical: 45,
        }}>
        <Image
          style={{width: 20, height: 20}}
          source={require('../assets/icons/plus.png')}
        />
        <Text style={{marginLeft: 8, color: '#BB2427', fontWeight: '700'}}>
          Tambah Barang
        </Text>
      </TouchableOpacity>
      {/* </ScrollView> */}
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Detail Cart')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Selanjutnya
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Cartscreen;
