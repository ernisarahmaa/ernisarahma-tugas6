import moment from 'moment/moment';
import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';

const AddData = ({navigation, route}) => {
  const [name, setName] = useState('');
  const [rating, setRating] = useState('');
  const [address, setAddress] = useState('');
  const [jamBuka, setJamBuka] = useState('');
  const [jamTutup, setJamTutup] = useState('');
  const [hargaMin, setHargaMin] = useState('');
  const [hargaMax, setHargaMax] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [gambar, setGambar] = useState('');
  const [isSelected, setSelection] = useState('');

  const toggleSelection = selectName => {
    setSelection(prevState => (prevState === selectName ? '' : selectName));
  };
  const dispatch = useDispatch();
  const {store} = useSelector(state => state.store);

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setName(data.nameStore);
      setRating(data.rating);
      setAddress(data.address);
      setJamBuka(data.jamBuka);
      setJamTutup(data.jamTutup);
      setHargaMin(data.hargaMin);
      setHargaMax(data.hargaMax);
      setDeskripsi(data.deskripsi);
      setGambar(data.gambar);
    }
  };

  React.useEffect(() => {
    checkData();
  }, []);

  const storeData = () => {
    var date = new Date();
    var dataStore = [...store];
    const data = {
      id: moment(date).format('YYYYMMDDHHmmssSSS'),
      nameStore: name,
      rating: rating,
      address: address,
      jamBuka: jamBuka,
      jamTutup: jamTutup,
      favourite: false,
      hargaMin: hargaMin,
      hargaMax: hargaMax,
      deskripsi: deskripsi,
      gambar: gambar,
      isOpen: isSelected === 'buka',
    };
    dataStore.push(data);
    dispatch({type: 'STORE_ADD_DATA', data: dataStore});
    navigation.goBack();
  };

  const updateData = () => {
    const data = {
      id: route.params.item.id,
      nameStore: name,
      rating: rating,
      address: address,
      jamBuka: jamBuka,
      jamTutup: jamTutup,
      favourite: false,
      hargaMin: hargaMin,
      hargaMax: hargaMax,
      deskripsi: deskripsi,
      gambar: gambar,
      isOpen: isSelected === 'buka',
    };
    dispatch({type: 'STORE_UPDATE_DATA', data});
    navigation.goBack();
  };

  const deleteData = async () => {
    dispatch({type: 'STORE_DELETE_DATA', id: route.params.item.id});
    navigation.goBack();
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff', paddingHorizontal: 15}}>
      <ScrollView>
        <View>
          <Text style={styles.title}>Nama Toko</Text>
          <TextInput
            placeholder="Masukkan Nama Toko"
            style={styles.txtInput}
            value={name}
            onChangeText={text => setName(text)}
          />
        </View>
        <View>
          <Text style={styles.title}>Alamat</Text>
          <TextInput
            placeholder="Masukkan Alamat"
            style={styles.txtInput}
            value={address}
            onChangeText={text => setAddress(text)}
          />
        </View>

        <View>
          <Text style={styles.title}>Status Buka Tutup</Text>
          <View style={{flexDirection: 'row'}}>
            <View style={{flexDirection: 'row', width: '53%'}}>
              <Icon
                name={isSelected === 'buka' ? 'check-circle' : 'circle-o'}
                size={20}
                style={{marginRight: 10}}
                onPress={() => toggleSelection('buka')}
              />
              <Text>Buka</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name={isSelected === 'tutup' ? 'check-circle' : 'circle-o'}
                size={20}
                style={{marginRight: 10}}
                onPress={() => toggleSelection('tutup')}
              />
              <Text>Tutup</Text>
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row'}}>
          <View style={{width: '53%'}}>
            <Text style={styles.title}>Jam Buka</Text>
            <TextInput
              style={{
                width: '85%',
                borderRadius: 6,
                borderColor: '#dedede',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              value={jamBuka}
              onChangeText={text => setJamBuka(text)}
            />
          </View>
          <View style={{width: '55%'}}>
            <Text style={styles.title}>Jam Tutup</Text>
            <TextInput
              style={{
                width: '85%',
                borderRadius: 6,
                borderColor: '#dedede',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              value={jamTutup}
              onChangeText={text => setJamTutup(text)}
            />
          </View>
        </View>

        <View>
          <Text style={styles.title}>Jumlah Rating</Text>
          <TextInput
            placeholder="Masukkan Jumlah Rating"
            style={styles.txtInput}
            value={rating}
            onChangeText={text => setRating(text)}
          />
        </View>

        <View style={{flexDirection: 'row'}}>
          <View style={{width: '53%'}}>
            <Text style={styles.title}>Harga Minimal</Text>
            <TextInput
              style={{
                width: '85%',
                borderRadius: 6,
                borderColor: '#dedede',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              value={hargaMin}
              onChangeText={text => setHargaMin(text)}
            />
          </View>
          <View style={{width: '55%'}}>
            <Text style={styles.title}>Harga Maksimal</Text>
            <TextInput
              style={{
                width: '85%',
                borderRadius: 6,
                borderColor: '#dedede',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              value={hargaMax}
              onChangeText={text => setHargaMax(text)}
            />
          </View>
        </View>

        <View>
          <Text style={styles.title}>Deskripsi</Text>
          <TextInput
            placeholder="Masukkan Deskripsi"
            style={styles.txtInput}
            value={deskripsi}
            onChangeText={text => setDeskripsi(text)}
          />
        </View>

        <View>
          <Text style={styles.title}>Gambar Toko</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
              borderRadius: 8,
              borderColor: '#dedede',
              borderWidth: 1,
            }}>
            <Icon name="image" size={40} style={{marginHorizontal: 7}} />
            <TextInput
              placeholder="Pilih Gambar"
              style={{flex: 1}}
              value={gambar}
              onChangeText={text => setGambar(text)}
            />
          </View>
        </View>
      </ScrollView>

      <View>
        <TouchableOpacity
          style={{
            width: '100%',
            marginVertical: 20,
            backgroundColor: '#4b5c6b',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            if (route.params) {
              updateData();
            } else {
              storeData();
            }
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            {route.params ? 'Ubah' : 'Simpan'}
          </Text>
        </TouchableOpacity>

        {route.params && (
          <TouchableOpacity
            style={{
              width: '100%',
              marginVertical: 15,
              backgroundColor: '#4b5c6b',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={deleteData}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Hapus
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    marginVertical: 10,
  },
  btnAdd: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#43a047',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  txtInput: {
    width: '100%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
});

export default AddData;
