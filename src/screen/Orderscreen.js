import moment from 'moment/moment';
import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';

const Orderscreen = ({navigation, route}) => {
  const [merk, setMerk] = useState('');
  const [color, setColor] = useState('');
  const [size, setSize] = useState('');
  const [note, setNote] = useState('');
  const [imageProduct, setImageProduct] = useState('');
  const [isService, setService] = useState('');

  const toggleSelection = serviceName => {
    setService(prevState => (prevState === serviceName ? '' : serviceName));
  };

  const dispatch = useDispatch();
  const {order} = useSelector(state => state.order);

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      console.log('data : ', data);
      setMerk(data.merk);
      setColor(data.color);
      setSize(data.size);
      setNote(data.note);
      setImageProduct(data.imageProduct);
    }
  };

  React.useEffect(() => {
    checkData();
  }, []);

  const storeData = () => {
    var dataOrder = [...order];
    const data = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      merk: merk,
      color: color,
      size: size,
      note: note,
      imageProduct: imageProduct,
      isService: isService,
    };
    dataOrder.push(data);
    dispatch({type: 'CART_ADD_DATA', data: dataOrder});
    navigation.navigate('Cart');
  };

  const updateData = () => {
    const data = {
      idCart: route.params.item.idCart,
      merk: merk,
      color: color,
      size: size,
      note: note,
      imageProduct: imageProduct,
      isService: isService,
    };
    dispatch({type: 'CART_UPDATE_DATA', data});
    navigation.goBack();
  };

  const deleteData = async () => {
    dispatch({
      type: 'CART_DELETE_DATA',
      idCart: route.params.item.idCart,
    });
    navigation.navigate('Cart');
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            paddingHorizontal: 20,
            marginTop: 5,
            backgroundColor: '#fff',
          }}>
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Merek
          </Text>
          <TextInput
            placeholder="Masukkan Merk Barang"
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            value={merk}
            onChangeText={text => setMerk(text)}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth : Merah - Putih "
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            value={color}
            onChangeText={text => setColor(text)}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S, M, L / 39,40"
            style={{
              marginTop: 10,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            value={size}
            onChangeText={text => setSize(text)}
          />
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Photo
          </Text>
          <TouchableOpacity style={styles.menuItem}>
            <Image
              style={styles.menuIcon}
              source={require('../assets/icons/camera.png')}
            />
            <Text style={styles.menuText}>Add Photo</Text>
          </TouchableOpacity>
          <View style={styles.checkboxContainer}>
            <Icon
              name={isService === 'gantiSol' ? 'check-square' : 'square-o'}
              size={20}
              style={styles.checkbox}
              onPress={() => toggleSelection('gantiSol')}
            />
            <Text style={styles.label}>Ganti Sol Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <Icon
              name={isService === 'jahitSepatu' ? 'check-square' : 'square-o'}
              size={20}
              style={styles.checkbox}
              onPress={() => toggleSelection('jahitSepatu')}
            />
            <Text style={styles.label}>Jahit Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <Icon
              name={isService === 'repaintSepatu' ? 'check-square' : 'square-o'}
              size={20}
              style={styles.checkbox}
              onPress={() => toggleSelection('repaintSepatu')}
            />
            <Text style={styles.label}>Repaint Sepatu</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <Icon
              name={isService === 'cuciSepatu' ? 'check-square' : 'square-o'}
              size={20}
              style={styles.checkbox}
              onPress={() => toggleSelection('cuciSepatu')}
            />
            <Text style={styles.label}>Cuci Sepatu</Text>
          </View>
          <Text
            style={{
              marginTop: 25,
              color: '#BB2427',
              fontWeight: 'bold',
            }}>
            Catatan
          </Text>
          <TextInput
            placeholder="Cth : ingin ganti sol baru"
            style={{
              marginTop: 10,
              width: '100%',
              height: 95,
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
              textAlignVertical: 'top',
            }}
            value={note}
            onChangeText={text => setNote(text)}
          />
          <View>
            <TouchableOpacity
              style={{
                width: '100%',
                marginVertical: 20,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                if (route.params) {
                  updateData();
                } else {
                  storeData();
                }
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                {route.params ? 'Ubah' : 'Masukkan Keranjang'}
              </Text>
            </TouchableOpacity>

            {route.params && (
              <TouchableOpacity
                style={{
                  width: '100%',
                  marginBottom: 15,
                  backgroundColor: '#BB2427',
                  borderRadius: 8,
                  paddingVertical: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={deleteData}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 16,
                    fontWeight: 'bold',
                  }}>
                  Hapus
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  menuItem: {
    width: 95,
    height: 95,
    borderRadius: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#BB2427',
    marginTop: 20,
    marginBottom: 40,
  },
  menuIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  menuText: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 5,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 8,
  },
});
export default Orderscreen;
