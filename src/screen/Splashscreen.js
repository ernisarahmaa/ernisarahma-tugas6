import React, {useEffect} from 'react';
import {View, Image} from 'react-native';

const Splashscreen = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 1000);
  });

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
      }}>
      <Image
        source={require('../assets/images/splashscreen.png')}
        style={{
          width: 230,
          height: 238,
        }}
      />
    </View>
  );
};

export default Splashscreen;
