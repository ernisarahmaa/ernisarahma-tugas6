import moment from 'moment';
import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

const Register = ({navigation, route}) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const dispatch = useDispatch();
  const {register} = useSelector(state => state.register);

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setName(data.name);
      setEmail(data.email);
      setPhoneNumber(data.phoneNumber);
      setPassword(data.password);
    }
  };

  React.useEffect(() => {
    checkData();
  }, []);

  const storeData = () => {
    if (password === confirmPassword) {
      var dataRegister = [...register];
      const data = {
        id: moment(new Date()).format('HHmmss'),
        name: name,
        email: email,
        phoneNumber: phoneNumber,
        password: password,
      };
      dataRegister.push(data);
      dispatch({type: 'NEW_REGISTER', data: dataRegister});

      Alert.alert(
        'Registration Successful',
        'You have successfully registered.',
        [
          {
            text: 'OK',
            onPress: () => {
              navigation.goBack();
            },
          },
        ],
      );
    } else {
      alert('Password and Confirm Password do not match.');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/images/bg.png')} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}
          />
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -125,
            }}>
            <Text
              style={{
                color: '#0A0827',
                fontWeight: '700',
                fontSize: 24,
                marginBottom: 25,
              }}>
              Welcome,{'\n'}Please Register
            </Text>
            <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Nama</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Enter Your Name" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              value={name}
              onChangeText={text => setName(text)}
            />
            <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Email</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Youremail@mail.com" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              value={email}
              onChangeText={text => setEmail(text)}
              keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
            <Text style={{color: '#BB2427', fontWeight: 'bold', marginTop: 15}}>
              Password
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data password
              placeholder="Enter password"
              secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              value={password}
              onChangeText={text => setPassword(text)}
            />
            <Text style={{color: '#BB2427', fontWeight: 'bold', marginTop: 15}}>
              Confirm Password
            </Text>
            <TextInput //component yang digunakan untuk memasukkan data password
              placeholder="Re-entered password"
              secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
              style={{
                marginTop: 15,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              value={confirmPassword}
              onChangeText={text => setConfirmPassword(text)}
            />
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icons/email.png')} //load asset dari local
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icons/facebook.png')}
                    style={{
                      width: 20,
                      height: 20,
                      marginHorizontal: 15,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icons/twitter.png')}
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#717171',
                  }}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={storeData}
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 41,
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Register
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 39,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: '#717171',
                }}>
                Already have an account?
              </Text>
              <TouchableOpacity>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#BB2427',
                    marginLeft: 5,
                  }}>
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};
export default Register;
