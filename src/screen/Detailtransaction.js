import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';

const DetailTransactionscreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: '#FFF',
          paddingTop: 15,
          paddingHorizontal: 25,
          paddingBottom: 25,
          justifyContent: 'space-between',
          height: 240,
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <Text style={{marginRight: 11, color: '#BDBDBD'}}>
            20 Desember 2020
          </Text>
          <Text style={{color: '#BDBDBD'}}>09:00</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text style={{color: '#201F26', fontWeight: '700', fontSize: 36}}>
            CS122001
          </Text>
          <Text style={{color: '#000'}}>Kode Reservasi</Text>
        </View>
        <Text style={{fontSize: 18, textAlign: 'center'}}>
          Sebutkan Kode Reservasi saat{'\n'}tiba di outlet
        </Text>
      </View>
      <View style={{paddingHorizontal: 11}}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: '500',
            color: '#201F26',
            marginVertical: 15,
          }}>
          Barang
        </Text>
        <View
          style={{
            backgroundColor: '#fff',
            flexDirection: 'row',
            paddingVertical: 25,
            borderRadius: 8,
            marginBottom: 20,
          }}>
          <Image
            style={{width: 85, height: 85, marginHorizontal: 20}}
            source={require('../assets/images/shoes1.png')}
          />
          <View style={{justifyContent: 'space-evenly'}}>
            <Text style={{fontWeight: '500', color: '#000'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text>Cuci Sepatu</Text>
            <Text>Note : -</Text>
          </View>
        </View>
        <Text
          style={{
            fontSize: 14,
            fontWeight: '500',
            color: '#201F26',
            marginVertical: 15,
          }}>
          Status Pesanan
        </Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('TransactionNavigation', {screen: 'Checkout'})
          }
          style={{
            backgroundColor: '#fff',
            flexDirection: 'row',
            paddingVertical: 25,
            borderRadius: 8,
            marginBottom: 35,
          }}>
          <Image
            style={{width: 14, height: 14, marginHorizontal: 18, marginTop: 3}}
            source={require('../assets/icons/status.png')}
          />
          <View style={{justifyContent: 'space-around'}}>
            <Text style={{fontSize: 14, fontWeight: '500', color: '#201F26'}}>
              Telah Reservasi
            </Text>
            <Text style={{fontSize: 10, color: '#A5A5A5'}}>
              20 Desember 2020
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content2: {
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  searchbar: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    backgroundColor: '#F6F8FF',
    borderRadius: 8,
  },
  filter: {
    backgroundColor: '#F6F8FF',
    width: 45,
    height: 45,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8,
  },
  container2: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
  },
  menuItem: {
    width: 95,
    height: 95,
    borderRadius: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuIcon: {
    width: 45,
    height: 45,
  },
  menuText: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 5,
  },
});

export default DetailTransactionscreen;
