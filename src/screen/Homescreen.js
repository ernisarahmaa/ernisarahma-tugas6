import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import {FlatList} from 'react-native-gesture-handler';

const Homescreen = ({navigation, route}) => {
  const [isLiked, setIsLiked] = useState(false);

  const toggleLike = () => {
    setIsLiked(!isLiked);
  };
  const {store} = useSelector(state => state.store);
  console.log('cek store', store);
  // const currentUser = useSelector(state => state.currentUser);
  // console.log('cek store', currentUser);

  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View style={styles.container1}>
        <View style={styles.content1}>
          <Image
            style={{width: 50, height: 50}}
            source={require('../assets/icons/profileuser.png')}
          />
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'Cart'})
            }>
            <Image
              style={{width: 20, height: 23}}
              source={require('../assets/icons/cart.png')}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            color: '#034262',
            fontSize: 23,
            fontWeight: '500',
            marginTop: 8,
          }}>
          Hello, Agil!
        </Text>
        <Text
          style={{
            color: '#0A0827',
            fontSize: 30,
            fontWeight: '700',
            paddingRight: 30,
            marginTop: 10,
          }}>
          Ingin merawat dan perbaiki sepatumu? cari disini
        </Text>
        <View style={styles.content2}>
          <View style={styles.searchbar}>
            <Image
              style={{marginLeft: 10, width: 17, height: 17}}
              source={require('../assets/icons/search.png')}
            />
            <TextInput
              style={{
                flex: 1,
                height: 45,
                paddingHorizontal: 10,
                color: '#000',
              }}
            />
          </View>
          <TouchableOpacity style={styles.filter}>
            <Image
              style={{width: 17, height: 17}}
              source={require('../assets/icons/filter.png')}
            />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.container2}>
        <TouchableOpacity style={styles.menuItem}>
          <Image
            style={styles.menuIcon}
            source={require('../assets/icons/shoes.png')}
          />
          <Text style={styles.menuText}>Sepatu</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem}>
          <Image
            style={styles.menuIcon}
            source={require('../assets/icons/jacket.png')}
          />
          <Text style={styles.menuText}>Jaket</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem}>
          <Image
            style={styles.menuIcon}
            source={require('../assets/icons/bag.png')}
          />
          <Text style={styles.menuText}>Tas</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.container3}>
        <Text style={{color: '#0A0827', fontSize: 18, fontWeight: '600'}}>
          Rekomendasi Terdekat
        </Text>
        <TouchableOpacity>
          <Text style={{color: '#E64C3C', fontSize: 16, fontWeight: '500'}}>
            View All
          </Text>
        </TouchableOpacity>
      </View>

      <FlatList
        style={styles.container4}
        data={store}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeNavigation', {
                screen: 'Detail',
                params: {item},
              })
            }
            onLongPress={() => {
              Alert.alert('Edit Data Store', 'Apakah Anda yakin?', [
                {
                  text: 'No',
                  style: 'cancel',
                },
                {
                  text: 'Yes',
                  style: 'destructive',
                  onPress: () =>
                    navigation.navigate('HomeNavigation', {
                      screen: 'Add_Data',
                      params: {item},
                    }),
                },
              ]);
            }}
            style={styles.list}>
            <Image
              style={{width: 100, height: 150}}
              source={{uri: item.gambar}}
            />
            <View
              style={{
                flex: 1,
                marginLeft: 13,
                justifyContent: 'space-evenly',
              }}>
              <Image
                style={{width: 50, height: 10, resizeMode: 'contain'}}
                source={require('../assets/icons/stars.png')}
              />
              <Text style={styles.shopRating}>{item.rating} Ratings</Text>
              <Text style={styles.shopName}>{item.nameStore}</Text>
              <Text style={styles.shopAddress}>{item.address}</Text>
              <View style={item.isOpen ? styles.shopOpen : styles.shopClose}>
                <Text style={item.isOpen ? styles.textOpen : styles.textClose}>
                  {item.isOpen ? 'BUKA' : 'TUTUP'}
                </Text>
              </View>
            </View>
            <TouchableOpacity onPress={toggleLike}>
              <Image
                style={{width: 14, height: 13, resizeMode: 'contain'}}
                source={
                  isLiked
                    ? require('../assets/icons/heartfilled.png')
                    : require('../assets/icons/heartempty.png')
                }
              />
            </TouchableOpacity>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnFloating}
        onPress={() =>
          navigation.navigate('HomeNavigation', {screen: 'Add_Data'})
        }>
        <Icon name="plus" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container1: {
    backgroundColor: '#FFF',
    paddingTop: 56,
    paddingHorizontal: 25,
    paddingBottom: 35,
  },
  content1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content2: {
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  searchbar: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    backgroundColor: '#F6F8FF',
    borderRadius: 8,
  },
  filter: {
    backgroundColor: '#F6F8FF',
    width: 45,
    height: 45,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8,
  },
  container2: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
  },
  menuItem: {
    width: 95,
    height: 95,
    borderRadius: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuIcon: {
    width: 45,
    height: 45,
  },
  menuText: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 5,
  },
  container3: {
    marginTop: 27,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
  },
  container4: {
    paddingHorizontal: 25,
    marginTop: 25,
  },
  list: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 6,
    borderRadius: 8,
    marginBottom: 5,
  },
  shopRating: {
    color: '#D8D8D8',
    fontSize: 14,
    fontWeight: '500',
  },
  shopName: {
    color: '#201F26',
    fontSize: 15,
    fontWeight: '600',
    marginTop: 5,
  },
  shopAddress: {
    color: '#D8D8D8',
    fontSize: 13,
    fontWeight: '500',
  },
  shopClose: {
    backgroundColor: '#FADBD8',
    width: 58,
    height: 25,
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: 10,
  },
  textClose: {
    color: '#EA3D3D',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '700',
  },
  shopOpen: {
    backgroundColor: '#DFEEE5',
    width: 58,
    height: 25,
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: 10,
  },
  textOpen: {
    color: '#2CAD60',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '700',
  },
  btnFloating: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Homescreen;
