import React, {useState} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';

const FAQscreen = ({navigation, route}) => {
  const [expanded, setExpanded] = useState(Array(6).fill(false)); // Initialize all questions as collapsed

  const toggleExpansion = index => {
    const newExpanded = [...expanded];
    newExpanded[index] = !newExpanded[index];
    setExpanded(newExpanded);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 6,
        paddingVertical: 33,
      }}>
      {Array(6)
        .fill()
        .map((_, index) => (
          <View
            key={index}
            style={{
              backgroundColor: '#FFF',
              flexDirection: 'column', // Change to column for dropdown
              marginBottom: 16,
            }}>
            <TouchableOpacity onPress={() => toggleExpansion(index)}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    marginVertical: 16,
                    marginHorizontal: 14,
                    color: '#000',
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit?
                </Text>
                <Image
                  style={{
                    width: 24,
                    height: 24,
                    alignSelf: 'center',
                    transform: [{rotate: expanded[index] ? '180deg' : '0deg'}],
                  }}
                  source={require('../assets/icons/chevron_down.png')}
                />
              </View>
            </TouchableOpacity>
            {expanded[index] && (
              <Text
                style={{
                  marginVertical: 16,
                  marginHorizontal: 14,
                  color: '#595959',
                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit?
              </Text>
            )}
          </View>
        ))}
    </View>
  );
};

export default FAQscreen;
