import React from 'react';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

const Detailscreen = ({navigation, route}) => {
  const {item} = route.params;

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <ImageBackground
          source={{uri: item.gambar}}
          resizeMode="cover"
          style={{height: 317}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 25,
              marginHorizontal: 20,
            }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('BottomNavigation', {screen: 'Home'})
              }>
              <Image
                style={{width: 35, height: 35, tintColor: 'white'}}
                source={require('../assets/icons/back.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
              <Image
                source={require('../assets/icons/cart.png')}
                style={{
                  width: 25,
                  height: 30,
                  tintColor: 'white',
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>

        <View
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingTop: 38,
            marginTop: -20,
          }}>
          <View
            style={{
              paddingHorizontal: 20,
            }}>
            <Text
              style={{
                color: '#0A0827',
                fontWeight: '700',
                fontSize: 24,
                marginBottom: 5,
              }}>
              {item.nameStore}
            </Text>
            <Image
              style={{width: '19%', height: 15, resizeMode: 'contain'}}
              source={require('../assets/icons/stars.png')}
            />
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
              }}>
              <View>
                <Image
                  style={{width: 20, height: 20}}
                  source={require('../assets/icons/location.png')}
                />
              </View>
              <View style={{width: '60%', marginLeft: 10}}>
                <Text>{item.address}</Text>
              </View>
              <TouchableOpacity
                style={{justifyContent: 'center', marginLeft: 'auto'}}>
                <Text
                  style={{color: '#3471CD', fontWeight: '700', fontSize: 15}}>
                  Lihat Maps
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                alignItems: 'center',
              }}>
              <View style={item.isOpen ? styles.shopOpen : styles.shopClose}>
                <Text style={item.isOpen ? styles.textOpen : styles.textClose}>
                  {item.isOpen ? 'BUKA' : 'TUTUP'}
                </Text>
              </View>
              <Text
                style={{
                  color: '#343434',
                  fontSize: 15,
                  fontWeight: '700',
                  marginLeft: 25,
                }}>
                {item.jamBuka} - {item.jamTutup}
              </Text>
            </View>
          </View>

          <View
            style={{
              borderTopWidth: 1,
              borderColor: '#EEEEEE',
              marginTop: 15,
            }}>
            <View
              style={{
                paddingHorizontal: 20,
              }}>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 25,
                }}>
                Deskripsi
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  marginTop: 13,
                }}>
                {item.deskripsi}
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 25,
                }}>
                Range Biaya
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '500',
                  marginTop: 5,
                }}>
                Rp {item.hargaMin} - {item.hargaMax}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            width: '100%',
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Order Form')}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Repair Disini
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  shopOpen: {
    backgroundColor: '#DFEEE5',
    width: 58,
    height: 25,
    borderRadius: 15,
    justifyContent: 'center',
  },
  shopClose: {
    backgroundColor: '#FADBD8',
    width: 58,
    height: 25,
    borderRadius: 15,
    justifyContent: 'center',
  },
  textOpen: {
    color: '#2CAD60',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '700',
  },
  textClose: {
    color: '#EA3D3D',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '700',
  },
});
export default Detailscreen;
