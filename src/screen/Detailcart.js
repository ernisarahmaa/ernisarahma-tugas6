import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

const DetailCartscreen = ({navigation, route}) => {
  // const {item} = route.params;

  const {order} = useSelector(state => state.order);
  console.log('cek cart', order);

  const serviceTextMap = {
    gantiSol: 'Ganti Sol Sepatu',
    jahitSepatu: 'Jahit Sepatu',
    repaintSepatu: 'Repaint Sepatu',
    cuciSepatu: 'Cuci Sepatu',
  };

  // const handleReservasi = () => {
  //   const reservasiData = {
  //     fullName: 'Agil Bani',
  //     phoneNumber: '0813763476',
  //     address: 'Jl. Perumnas, Condong catur, Sleman, Yogyakarta',
  //     storeName: 'Jack Repair - Seturan',
  //     storeId: '2',
  //     storeNumber: '027-343457',
  //     date: new Date(),
  //     reservationCode: generateUniqueCode(),
  //   };
  //   dispatch(addReservation(reservasiData));
  //   navigation.navigate('Transaction');
  // };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Data Customer
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>Agil Bani</Text>
            <Text style={{color: '#201F26'}}>(0813763476)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{color: '#201F26'}}>gantengdoang@dipanggang.com</Text>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Alamat Outlet Tujuan
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>
              Jack Repair - Seturan
            </Text>
            <Text style={{color: '#201F26'}}>(027-343457)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>Barang</Text>
          <View style={{marginTop: 9}}>
            {order.map((item, index) => (
              <View
                key={index}
                style={{
                  flexDirection: 'row',
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  marginBottom: 12,
                }}>
                <Image
                  style={{width: 85, height: 85}}
                  source={require('../assets/images/shoes1.png')}
                />
                <View
                  style={{
                    justifyContent: 'space-evenly',
                    marginLeft: 13,
                    flex: 1,
                  }}>
                  <Text style={{fontWeight: '500', color: '#000'}}>
                    {item.merk} - {item.color} - {item.size}
                  </Text>
                  <Text>{serviceTextMap[item.isService]}</Text>
                  <Text>Note: {item.note}</Text>
                </View>
              </View>
            ))}
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Reservasi')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DetailCartscreen;
