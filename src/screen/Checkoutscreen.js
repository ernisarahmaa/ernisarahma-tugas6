import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {Component, useState} from 'react';

const DATA = [
  {
    id: 'Bank Transfer',
    image: require('../assets/icons/transfer.png'),
  },
  {
    id: 'OVO',
    image: require('../assets/icons/ovo.png'),
  },
  {
    id: 'Kartu Kredit',
    image: require('../assets/icons/credit.png'),
  },
];

const Item = ({image, id, onPress, selected}) => (
  <TouchableOpacity
    onPress={onPress}
    style={{
      borderColor: '#E1E1E1',
      borderWidth: 1,
      borderRadius: 3,
      width: 126,
      height: 82,
      marginVertical: 8,
      marginHorizontal: 6,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: selected ? 'rgba(3, 66, 98, 0.16)' : '#fff',
    }}>
    <Image
      source={image}
      style={{
        width: id === 'OVO' ? 60 : 40,
        height: 40,
        resizeMode: 'contain',
      }}
    />
    {selected && (
      <Image
        source={require('../assets/icons/checkmark.png')}
        style={{
          position: 'absolute',
          top: 6,
          right: 6,
          width: 20,
          height: 20,
          resizeMode: 'contain',
        }}
      />
    )}
    <Text
      style={{
        color: 'black',
        fontWeight: '400',
        paddingTop: 4,
      }}>
      {id}
    </Text>
  </TouchableOpacity>
);

const Checkoutscreen = () => {
  const [selectedId, setSelectedId] = useState();

  const renderItem = ({item}) => {
    const selected = item.id === selectedId;

    return (
      <Item
        image={item.image}
        id={item.id}
        onPress={() => setSelectedId(item.id)}
        selected={selected}
      />
    );
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Data Customer
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>Agil Bani</Text>
            <Text style={{color: '#201F26'}}>(0813763476)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{color: '#201F26'}}>gantengdoang@dipanggang.com</Text>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Alamat Outlet Tujuan
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{marginRight: 10, color: '#201F26'}}>
              Jack Repair - Seturan
            </Text>
            <Text style={{color: '#201F26'}}>(027-343457)</Text>
          </View>
          <Text style={{color: '#201F26'}}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>Barang</Text>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{width: 85, height: 85}}
              source={require('../assets/images/shoes1.png')}
            />
            <View style={{justifyContent: 'space-evenly', marginLeft: 13}}>
              <Text style={{fontWeight: '500', color: '#000'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text>Cuci Sepatu</Text>
              <Text>Note : -</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 16,
            }}>
            <Text style={{color: '#000', fontWeight: '400', fontSize: 16}}>
              1 Pasang
            </Text>
            <Text style={{color: '#000', fontWeight: '700', fontSize: 16}}>
              @Rp 50.000
            </Text>
          </View>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 25,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{marginBottom: 10, fontWeight: '500'}}>
            Rincian Pembayaran
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{marginRight: 10, color: '#201F26'}}>
                Cuci Sepatu
              </Text>
              <Text style={{color: '#FFC107', fontWeight: '500'}}>
                x1 Pasang
              </Text>
            </View>
            <Text>Rp 30.000</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 1,
              borderColor: '#000',
              paddingBottom: 10,
            }}>
            <Text style={{color: '#201F26'}}>Biaya Antar</Text>
            <Text style={{color: '#201F26'}}>Rp 3.000</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingTop: 8,
            }}>
            <Text style={{color: '#201F26'}}>Total</Text>
            <Text style={{color: '#034262', fontWeight: '700'}}>Rp 33.000</Text>
          </View>
        </View>
        <View
          style={{
            marginBottom: 12,
            backgroundColor: '#fff',
            padding: 19,
            justifyContent: 'center',
            borderRadius: 8,
          }}>
          <Text style={{fontWeight: '500', paddingLeft: 6, paddingBottom: 8}}>
            Pilih Pembayaran
          </Text>
          <ScrollView showsHorizontalScrollIndicator={false}>
            <FlatList
              horizontal={true}
              data={DATA}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </ScrollView>
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 20,
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Reservasi')}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Pesan Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Checkoutscreen;
