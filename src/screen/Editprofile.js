import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';

const EditProfilescreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFF', paddingHorizontal: 20}}>
      <View style={{alignItems: 'center', marginVertical: 30}}>
        <Image
          style={{width: 95, height: 95}}
          source={require('../assets/icons/profileuser.png')}
        />
        <View style={{flexDirection: 'row', marginVertical: 20}}>
          <Image
            style={{width: 24, height: 24}}
            source={require('../assets/icons/edit.png')}
          />
          <Text>Edit Foto</Text>
        </View>
      </View>
      <View style={{flex: 1}}>
        <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Nama</Text>
        <TextInput
          placeholder="Agil Bani"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
            marginBottom: 25,
          }}
        />
        <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Email</Text>
        <TextInput
          placeholder="gilagil@gmail.com"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
            marginBottom: 25,
          }}
        />
        <Text style={{color: '#BB2427', fontWeight: 'bold'}}>No Hp</Text>
        <TextInput
          placeholder="08124564879"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
            marginBottom: 25,
          }}
        />
      </View>
      <View
        style={{
          marginBottom: 48,
        }}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#BB2427',
            height: 55,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() =>
            navigation.navigate('BottomNavigation', {screen: 'Profile'})
          }>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Simpan
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default EditProfilescreen;
