// const initialState = {
//   register: [],
// };

// const authReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case 'NEW_REGISTER':
//       return {
//         ...state,
//         register: action.data,
//       };
//     default:
//       return state;
//   }
// };

// export default authReducer;

const initialState = {
  register: [], // Data pengguna yang telah diregistrasi
  currentUser: null, // Menyimpan data pengguna yang saat ini masuk
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'NEW_REGISTER':
      return {
        ...state,
        register: action.data,
      };
    case 'LOGIN_USER':
      const user = state.register.find(value => value.id === action.data.id);
      if (user) {
        return {
          ...state,
          currentUser: user,
        };
      } else {
        return state; // Menyimpan state yang tidak berubah jika pengguna tidak ditemukan
      }

    case 'LOGOUT_USER':
      // Logika untuk logout pengguna
      return {
        ...state,
        currentUser: null,
      };
    default:
      return state;
  }
};

export default authReducer;
