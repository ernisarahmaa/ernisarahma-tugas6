const initialState = {
  order: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CART_ADD_DATA':
      return {
        ...state,
        order: action.data,
      };
    case 'CART_UPDATE_DATA':
      var newData = [...state.order];
      var findIndex = state.order.findIndex(value => {
        return value.idCart === action.data.idCart;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        order: newData,
      };
    case 'CART_DELETE_DATA':
      var newData = [...state.order];
      var findIndex = state.order.findIndex(value => {
        return value.idCart === action.idCart;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        order: newData,
      };
    default:
      return state;
  }
};

export default cartReducer;
