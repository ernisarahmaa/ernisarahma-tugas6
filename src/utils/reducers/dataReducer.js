const initialState = {
  store: [],
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'STORE_ADD_DATA':
      return {
        ...state,
        store: action.data,
      };
    case 'STORE_UPDATE_DATA':
      var newData = [...state.store];
      var findIndex = state.store.findIndex(value => {
        return value.id === action.data.id;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        store: newData,
      };
    case 'STORE_DELETE_DATA':
      var newData = [...state.store];
      var findIndex = state.store.findIndex(value => {
        return value.id === action.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        store: newData,
      };
    default:
      return state;
  }
};

export default dataReducer;
