import {combineReducers} from 'redux';
import dataReducer from './reducers/dataReducer';
import cartReducer from './reducers/cartReducer';
import authReducer from './reducers/authReducer';

const rootReducer = combineReducers({
  store: dataReducer,
  order: cartReducer,
  register: authReducer,
  currentUser: authReducer,
});

export default rootReducer;
